import wiringpi as gpio
from wiringpi import GPIO
KEY_PIN = 16

KEY_UP = True
count = 0

def scanKey(PIN,mode = 0):
    global KEY_UP
    if mode == 1:
        KEY_UP = True
    if( KEY_UP == True and gpio.digitalRead(PIN) == GPIO.LOW):
        KEY_UP = False
        gpio.delay(20)
        if(gpio.digitalRead(PIN) == GPIO.LOW):
            return 1
    #key release      
    if( KEY_UP == False and gpio.digitalRead(PIN) == GPIO.HIGH):
        KEY_UP = True

    return 0
                    

# One of the following MUST be called before using IO functions:
gpio.wiringPiSetupGpio()  # For GPIO pin numbering  使用BCM编号

gpio.pinMode(KEY_PIN, GPIO.INPUT)
#gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_OFF)
gpio.pullUpDnControl(KEY_PIN, GPIO.PUD_UP)
#gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_DOWN)
while 1:
    state = scanKey(KEY_PIN)
    if state == 1 :
        count = count + 1
        print("press",count)