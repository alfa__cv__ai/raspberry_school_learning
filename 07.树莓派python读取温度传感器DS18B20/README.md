# 07.树莓派python读取温度传感器DS18B20数据

​	Ds18b20是最常见的数字温度传感器，有着诸多优点，比如：体积小；精度高（12位时精度可以达到0.625摄氏度，出厂默认12位）；接线简单，无需外围短路；封装形式多样，能满足不同应用需求等等。但是，“鱼与熊掌不可兼得”，接线简单的同时，带来的问题便是传感器复杂的工作时序。Ds18b20工作时序十分复杂，信号传输只在微妙之间，稍有不慎（例如延时函数的使用不够恰当）就可能导致其整个传感器的工作时序混乱。因此，在写Ds18b20的控制程序时一定要弄清工作时序并使用合适的延时函数。
​	在linux下，这些都不用我们操心！！！

先测试DS18B20

参考一篇博客：http://www.waveshare.net/study/article-607-1.html

修改配置文件

```
sudo vi /boot/config.txt
```

在/boot/config.txt文件后面添加下面这一句，这一句就是树莓派添加Device Tree设备，dtoverlay=w1-gpio-pull表示添加单总线设备，gpiopin=4默认管脚为4，如果DS18B20接到其他管脚则需要修改这个值，Pioneer 600扩展板DS18B20默认接到4，故不用修改。（注：管脚为BCM编号）

```
dtoverlay=w1-gpio-pullup,gpiopin=4
```

重启树莓派

```
sudo reboot
```

重启后查看lsmod

```
lsmod
```

加载模块

```
sudo modprobe w1_gpio
sudo modprobe w1_therm
```

查看，能看到这两个说明加载好了

```
lsmod
```

![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01pUsPJX1LbgbNiaSfn_!!63891318.png)

打开以下路径

```
cd /sys/bus/w1/devices
```

输入ls，看里面的文件

![](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\1583679701248.png)

那么就28-后面的就是DS18B20的编号，每个传感器有唯一编号，我的是0516a2dbedff，

然后

```
cd 28-0516a2dbedff
cat w1_slave
```

![](https://img.alicdn.com/imgextra/i3/63891318/O1CN01sq6yEK1LbgbSJP7Vk_!!63891318.png)

获取到温度26度了，不容易啊！！！

终于上手代码了

```
#!/usr/bin/python3
import os,time

device_file ='/sys/bus/w1/devices/28-031681e171ff/w1_slave'

def read_temp_raw():
    f = open(device_file,'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string)/1000.0
    return temp_c

while True:
    print('temp C = %f'%read_temp())
    time.sleep(1)


```

以上例程配套的接线图

![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01H9yPs91LbgbNkULhY_!!63891318.png)

最终终于获取到温度值

![](https://img.alicdn.com/imgextra/i3/63891318/O1CN01yqRfRe1LbgbGIa15X_!!63891318.png)