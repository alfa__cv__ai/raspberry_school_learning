# 08.树莓派wiringPi python读取温度传感器DS18B20

看了下wiringPi的库，发现里面有ds18b20.c和ds18b20.h,真方便，剩下研究下怎么用就行了。

不过。。。可惜的是网上竟然没有一个这方面的教程，只能自己看源码了，还好源码不难

首先是看ds18b20.h源文件如下

```
#ifdef __cplusplus
extern "C" {
#endif

extern int ds18b20Setup (const int pinBase, const char *serialNum) ;

#ifdef __cplusplus
}
#endif
```

这里我们可以得到一个可以被调用的函数ds18b20Setup，从名字上看是一个初始化函数，两个输入参量，

一个是pinBase，这个是定义一个扩展IO，自己可以随便写，但是要大于64，我怎么知道呢，看看看源码+测试，没别的办法啊，虽然我的c语言是入门级的，但是也要硬着头皮看！！！

一个是serialNum从名字上看这个是一组串号，上一讲我们得到过这个串号，这个easy！

那么这个pinBase到底是什么呢，只能去看源码了ds18b20.c

```
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <ctype.h>

#include "wiringPi.h"

#include "ds18b20.h"

#define	W1_PREFIX	"/sys/bus/w1/devices/28-"
#define	W1_POSTFIX	"/w1_slave"


/*
 * myAnalogRead:
 *********************************************************************************
 */

static int myAnalogRead (struct wiringPiNodeStruct *node, int pin)
{
  int  chan = pin - node->pinBase ;
  int  fd = node->fd ;
  char buffer [4096] ;
  char *p ;
  int  temp, sign ;

  if (chan != 0)
    return -9999 ;

// Rewind the file - we're keeping it open to keep things going
//	smoothly

  lseek (fd, 0, SEEK_SET) ;

// Read the file - we know it's only a couple of lines, so this ought to be
//	more than enough

  if (read (fd, buffer, 4096) <= 0)	// Read nothing, or it failed in some odd way
    return -9998 ;

// Look for YES, then t=

  if (strstr (buffer, "YES") == NULL)
    return -9997 ;

  if ((p = strstr (buffer, "t=")) == NULL)
    return -9996 ;

// p points to the 't', so we skip over it...

  p += 2 ;

// and extract the number
//	(without caring about overflow)


  if (*p == '-')	// Negative number?
  {
    sign = -1 ;
    ++p ;
  }
  else
    sign = 1 ;

  temp = 0 ;
  while (isdigit (*p))
  {
    temp = temp * 10 + (*p - '0') ;
    ++p ;
  }

// We know it returns temp * 1000, but we only really want temp * 10, so
//	do a bit of rounding...

  temp = (temp + 50) / 100 ;
  return temp * sign ;
}


/*
 * ds18b20Setup:
 *	Create a new instance of a DS18B20 temperature sensor.
 *********************************************************************************
 */

int ds18b20Setup (const int pinBase, const char *deviceId)
{
  int fd ;
  struct wiringPiNodeStruct *node ;
  char *fileName ;

// Allocate space for the filename

  if ((fileName = malloc (strlen (W1_PREFIX) + strlen (W1_POSTFIX) + strlen (deviceId) + 1)) == NULL)
    return FALSE ;

  sprintf (fileName, "%s%s%s", W1_PREFIX, deviceId, W1_POSTFIX) ;

  fd = open (fileName, O_RDONLY) ;

  free (fileName) ;

  if (fd < 0)
    return FALSE ;

// We'll keep the file open, to make access a little faster
//	although it's very slow reading these things anyway )-:

  node = wiringPiNewNode (pinBase, 1) ;

  node->fd         = fd ;
  node->analogRead = myAnalogRead ;

  return TRUE ;
}

```

从ds18b20Setup函数的内部我们可以看出来，实际上这个里面的操作和我们上一讲的流程一样，只不过是用的c写的读取文件，有点区别是多了一个wiringPiNewNode的函数，这个函数是干嘛的，网上真是没有说明，只能自己猜测了，后面想了下，应该是这样理解，后面是c语言的链表结构，定义了一个以后可以被调用的函数analogRead，这个函数我们很熟悉和arduino里面的模拟量读取的是一样的，然后这个函数的底层是一个myAnalogRead函数，这个函数我们通过看源码发现就是把文件内的数据t=后面的数字提取出来，然后做了一个四舍五入，然后return出来，return的这个值是温度值的10倍！！！！而且带有符号。

好了，有了这些我们就可以去试试写代码了，我们知道2个可以用的函数，

一个是ds18b20Setup可以用于初始化一个扩展IO，这个IO可以自己定义

第二个是可以通过analogRead，读取到这个扩展IO的值，这个值是温度的值的10倍

完美！！！那么代码就很简单了

但是还有问题，这个是c代码里面的，python可以直接用吗，

答案显然是：OK!

wiringPi python版本已经把这些python调用c的声明都做好了，我们直接用就行了

以下就是python版本使用wiringPi读取DS18B20温度值的我写的测试源码

```
#!/usr/bin/python3
import wiringpi as gpio
from wiringpi import GPIO
SENSOR_PIN_BASE = 65

gpio.wiringPiSetupGpio()  # For GPIO pin numbering  使用BCM编号

state = gpio.ds18b20Setup(SENSOR_PIN_BASE,"0516a2dbedff")
print(state)
while True:
    if state:
        temp = gpio.analogRead(SENSOR_PIN_BASE)
        temp = temp/10
        print(temp,' c')
    else:
        print("DS18B20 setup error!")
    gpio.delay(1000)

```

以上例程配套的接线图

![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01H9yPs91LbgbNkULhY_!!63891318.png)

最终终于获取到温度值

![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01EHPXua1LbgbPAh846_!!63891318.png)

而且，今天发现这个树莓派里面有个比Thenny更好使的开发环境，Mu，还是不错的，目前发现这个Check，不错，可以检查代码的错误和代码工整性的提示

![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01hFaXly1LbgbOcQaOj_!!63891318.png)